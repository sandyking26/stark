package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC002_CreateLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_CreateLeads";
		testcaseDec = "Create Leads help us to create new User profile in Leads section";
		author = "Santhosh Kumar.V";
		category = "Functional";
		excelFileName = "TC002";
	} 

	@Test(dataProvider="fetchData") 
	public void createLead(String uName, String pwd,String Cname,String Fname,String Lname) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickCreateLeads()
		.enterCompanyName(Cname)
		.enterFirstName(Fname)
		.enterLastName(Lname)
		.clickCreateButton()
		.verifyFirstName(Fname);
		
		
	

}
}