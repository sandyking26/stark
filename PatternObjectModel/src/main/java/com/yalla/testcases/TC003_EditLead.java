package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC003_EditLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC003_EditLeads";
		testcaseDec = "Edit Leads help us to update existing User profile in Leads section";
		author = "Santhosh Kumar.V";
		category = "Functional";
		excelFileName = "TC003";
	} 

	@Test(dataProvider="fetchData") 
	public void createLead(String uName, String pwd,String Cname,String Fname,String Lname, String NCname,String NFname,String NLname) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickFindLeads()
		.enterFirstName(Fname)
		.clickFindLeads()
		.verifyFirstName(Fname)
		.clickFName()
		.clickEdit()
		.editCompnaytName(NCname)
		.editFirstName(NFname)
		.editLastName(NLname)
		.clickUpdate()
		.verifyFirstName(NFname);
		
		
	

}
}