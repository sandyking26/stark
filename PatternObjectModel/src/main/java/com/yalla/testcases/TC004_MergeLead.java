package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC004_MergeLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC004_MergeLeads";
		testcaseDec = "Merge Leads help us to update existing User profile in Leads section";
		author = "Santhosh Kumar.V";
		category = "Functional";
		excelFileName = "TC004";
	} 

	@Test(dataProvider="fetchData") 
	public void createLead(String uName, String pwd,String fromID,String toID,String recordInfo) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickMergeLeads()
		.clickFromLead()
		.enterLeadID(fromID)
		.clickFindLeads()
		.clickLeadID()
		.clickToLead()
		.enterLeadID(toID)
		.clickFindLeads()
		.clickLeadID()
		.clickMergeButton()
		.acceptMergeAlert()
		.clickFindLeads()
		.enterLeadID(fromID)
		.clickFindLeads()
		.verifyRecordExist(recordInfo);
		

}
}