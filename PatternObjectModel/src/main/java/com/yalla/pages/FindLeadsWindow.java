package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class FindLeadsWindow extends Annotations {
	public FindLeadsWindow() {
		PageFactory.initElements(driver, this);
	} 


	@FindBy(how =How.XPATH, using="//input[@name='id']") WebElement eleLeadID;

	@FindBy(how =How.XPATH, using="//button[@class='x-btn-text']") WebElement eleFindLeadButton;

	@FindBy(how =How.XPATH, using="//div[@class= 'x-grid3-cell-inner x-grid3-col-partyId']/a") WebElement eleFirstCell;
	@FindBy(how =How.XPATH, using="//div[@class= 'x-paging-info']") WebElement elePageInfo;


	public FindLeadsWindow enterLeadID(String data) {
		clearAndType(eleLeadID, data);
		return this;
	}


	public FindLeadsWindow clickFindLeads() {
		click(eleFindLeadButton);
		return this;
	}

	public FindLeadsWindow verifyLeadID(String data) {

		verifyExactText(eleFirstCell, data);

		return this; 
	}
	
	/*public FindLeadsWindow switchToWindowFindLeads() {
		switchToWindow(1);
		return this;
	}*/
  

	public MergeLeads clickLeadID() {
		clickWithNoSnap(eleFirstCell);
		switchToWindow(0);
		return new MergeLeads();
	}



}
