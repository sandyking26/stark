package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.Then;

public class ViewLeads extends Annotations {
	public ViewLeads() {
	       PageFactory.initElements(driver, this);
		} 

		
		@FindBy(how =How.ID, using="viewLead_firstName_sp") WebElement eleVerifyFname;
		@FindBy(how =How.XPATH, using="//a[contains(text(),'Edit')]") WebElement eleEdit;
		@FindBy(how=How.XPATH, using ="//a[text()='Find Leads']") WebElement eleFindLeads;
				
		@Then("Verify LeadTwo as (.*)")
		public ViewLeads verifyFirstName(String data) {
			
			verifyExactText(eleVerifyFname, data);
			
			return this; 
		}
		
		public EditLeads clickEdit() {
			click(eleEdit);
			return new EditLeads();
		}
		public FindLeads clickFindLeads() {
			click(eleFindLeads);
			return new FindLeads();
		}


}
