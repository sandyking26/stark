package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class FindLeads extends Annotations {
	public FindLeads() {
		PageFactory.initElements(driver, this);
	} 


	@FindBy(how =How.XPATH, using="//input[@name='id']") WebElement eleLeadID;

	@FindBy(how =How.XPATH, using="(//input[@name='firstName'])[3]") WebElement eleFname;
	@FindBy(how =How.XPATH, using="(//input[@name='lastName'])[3]") WebElement eleLname;
	@FindBy(how =How.XPATH, using="(//input[@name='companyName'])[2]") WebElement eleCname;



	@FindBy(how =How.XPATH, using="//input[@name='firstName']") WebElement eleVerifyFname;



	@FindBy(how =How.XPATH, using="//a[contains(text(),'Edit')]") WebElement eleEdit;
	@FindBy(how=How.XPATH, using ="//button[text()='Find Leads']") WebElement eleFindButton;

	@FindBy(how=How.XPATH, using="//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']/div/a") WebElement firstRowID;

	@FindBy(how=How.XPATH, using ="(//div[contains(@class,'firstName')]/a)[2]") WebElement firstRowFName;
	@FindBy(how=How.XPATH, using ="//div[@class= 'x-paging-info']") WebElement elepageInfo;


	
	
	public FindLeads enterLeadID(String data) {
		clearAndType(eleLeadID, data);
		return this;
	}
	public FindLeads enterFirstName(String data) {
		clearAndType(eleFname, data);
		return this;
	}
	public FindLeads enterLastName(String data) {
		clearAndType(eleLname, data);
		return this;
	}
	
	public FindLeads enterCompanyName(String data) {
		clearAndType(eleCname, data);
		return this;
	}
	
	public FindLeads clickFindLeads() {
		click(eleFindButton);
		return this;
	}
	public FindLeads verifyRecordExist(String data) {

		verifyExactText(elepageInfo, data);

		return this; 
	}
	

	public FindLeads verifyLeadID(String data) {

		verifyExactText(firstRowID, data);

		return this; 
	}
	public FindLeads verifyFirstName(String data) {
       verifyPartialText(firstRowFName, data);
		

		return this; 
	}

	public ViewLeads clickLeadID() {
		click(firstRowID);
		return new ViewLeads();
	}
	
	public ViewLeads clickFName() {
		click(firstRowFName);
		return new ViewLeads();
		
	}



}
