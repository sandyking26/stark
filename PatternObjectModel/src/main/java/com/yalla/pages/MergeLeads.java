package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class MergeLeads extends Annotations {
	public MergeLeads() {
		PageFactory.initElements(driver, this);
	} 


	@FindBy(how =How.XPATH, using="(//img[@src='/images/fieldlookup.gif'])[1]") WebElement eleFromImg;
	@FindBy(how =How.XPATH, using="(//img[@src='/images/fieldlookup.gif'])[2]") WebElement eleToImg;
	@FindBy(how =How.XPATH, using="//a[text()='Merge']") WebElement eleMergeButton;
	


	public FindLeadsWindow clickFromLead() {
		clickWithNoSnap(eleFromImg);
		switchToWindow(1);
		
		return new FindLeadsWindow();
	}
	
	public FindLeadsWindow clickToLead() {
		clickWithNoSnap(eleToImg);
		switchToWindow(1);
		return new FindLeadsWindow();
	}

	/*public FindLeadsWindow eleMergeButtonclickToLead() {
		click(eleToImg);
		
		return new FindLeadsWindow();
	}*/
	
	public MergeLeads clickMergeButton() {
		clickWithNoSnap(eleMergeButton);
		return this;
	}
  
	/*public MergeLeads switchToWindowMergeLeads() {
		switchToWindow(0);
		return this;
	}*/
	public ViewLeads acceptMergeAlert() {
		acceptAlert();
		return new ViewLeads();
	}
	
}
