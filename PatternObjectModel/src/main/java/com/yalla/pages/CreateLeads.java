package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

public class CreateLeads extends Annotations {
	public CreateLeads() {
	       PageFactory.initElements(driver, this);
		} 

		
		@FindBy(how =How.ID, using="createLeadForm_companyName") WebElement eleCompany;
		@FindBy(how =How.ID, using="createLeadForm_firstName") WebElement eleFname;
		@FindBy(how =How.ID, using="createLeadForm_lastName") WebElement eleLname;
		@FindBy(how=How.CLASS_NAME, using="smallSubmit") WebElement eleCreate;
		
		@And("Enter Company Name as (.*)")
		public CreateLeads enterCompanyName(String data) {
			
			clearAndType(eleCompany, data);  
			return this; 
		}
		@And("Enter First Name as (.*)")
		public CreateLeads enterFirstName(String data) {
			
			clearAndType(eleFname, data);  
			return this; 
		}
		@And("Enter Last Name as (.*)")
		public CreateLeads enterLastName(String data) {
			
			clearAndType(eleLname, data);  
			return this; 
		}
		
		@When("Click submitTwo")
		public ViewLeads clickCreateButton() {
			click(eleCreate);
			return new ViewLeads();
		}


}
