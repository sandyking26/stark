package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;

public class MyLeads extends Annotations {
	public MyLeads() {
	       PageFactory.initElements(driver, this);
		} 

		
		@FindBy(how =How.LINK_TEXT, using="Create Lead") WebElement eleCreateLead;
		@FindBy(how =How.LINK_TEXT, using="Find Leads") WebElement eleFindLead;
		@FindBy(how =How.LINK_TEXT, using="Merge Leads") WebElement eleMergeLead;
		
		@And("Click Create Leads")
		public CreateLeads clickCreateLeads() {
			click(eleCreateLead);
			return new CreateLeads();
		}
		
		public FindLeads clickFindLeads() {
			click(eleFindLead);
			return new FindLeads();
		}
		public MergeLeads clickMergeLeads() {
			click(eleMergeLead);
			return new MergeLeads();
		}


}
