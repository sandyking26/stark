package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class EditLeads extends Annotations {
	public EditLeads() {
		PageFactory.initElements(driver, this);
	} 


	@FindBy(how =How.ID, using="updateLeadForm_companyName") WebElement eleEditCompanyName;
	@FindBy(how =How.ID, using="updateLeadForm_firstName") WebElement eleEditFirstName;
	@FindBy(how =How.ID, using="updateLeadForm_lastName") WebElement eleEditLastName;

	@FindBy(how =How.CLASS_NAME, using="smallSubmit") WebElement eleUpdate;

	public EditLeads editCompnaytName(String data) {

		append(eleEditCompanyName, data);

		return this; 
	}

	public EditLeads editFirstName(String data) {

		append(eleEditFirstName, data);

		return this; 
	}
	public EditLeads editLastName(String data) {

		append(eleEditLastName, data);

		return this; 
	}

	public ViewLeads clickUpdate() {
		click(eleUpdate);
		return new ViewLeads();
	}



}
