package runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features="F:/SELENIUM/Softwares/PatternObjectModel/src/test/java/features",
                  glue= {"com.yalla.pages","steps"},
                  monochrome=true
//                dryRun=true,
  //                snippets= SnippetType.CAMELCASE
                  )

public class RunTest {
	
}
