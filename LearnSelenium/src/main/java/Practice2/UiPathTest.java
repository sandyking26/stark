package Practice2;

import java.util.List;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class UiPathTest {
	
	ChromeDriver driver;
	@DataProvider(name="data")
	public Object[][] fetchdata() {
		Object [] [] data = new Object[1][3];
		data[0][0]="senthilkumar0013@gmail.com";
		data[0][1]="klnklnkln";
	    data[0][2]="RO212121";
		return data;
	}
	
	
	@Test(dataProvider="data")
	public void displayVendor(String uname, String pwd, String id)
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://acme-test.uipath.com/account/login");
		
		driver.findElementById("email").sendKeys(uname);
		driver.findElementById("password").sendKeys(pwd);
		driver.findElementById("buttonLogin").click();
		
		Actions builder = new Actions(driver);
		builder.moveToElement(driver.findElementByXPath("//button[text()=' Vendors']")).pause(2000).build().perform();;
		builder.click(driver.findElementByLinkText("Search for Vendor")).perform();
		
		driver.findElementById("vendorTaxID").sendKeys(id);
		driver.findElementById("buttonSearch").click();
		
		WebElement table = driver.findElementByXPath("//table[@class='table']");
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		
		
			List<WebElement> columns = rows.get(1).findElements(By.tagName("td"));
			for (WebElement column : columns) {
				System.out.println(column.getText());
				
			}
			
		
		
		
		
		
		
		
	}
	
}
